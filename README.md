```
root@jw:~#docker build -t dind:1 .
root@jw:~# docker run --privileged -d c820f070fba1
09d4bd11caf38adbc72a7ba411f0549dbe5315b5692d74483204d1f1d872fac1
root@jw:~# docker exec -ti 09d4bd11caf38adbc72a7ba411f0549dbe5315b5692d74483204d1f1d872fac1 /bin/bash
root@09d4bd11caf3:/# cd /root && echo -e "FROM alpine:latest\nCMD [\"cat\", \"/etc/os-release\"]" > Dockerfile
root@09d4bd11caf3:~# docker build . | tail -n 1 | awk '{print $3}' | xargs docker run
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.12.1
PRETTY_NAME="Alpine Linux v3.12"
HOME_URL="https://alpinelinux.org/"
BUG_REPORT_URL="https://bugs.alpinelinux.org/"
root@09d4bd11caf3:~# cat /etc/os-release
PRETTY_NAME="Debian GNU/Linux 9 (stretch)"
NAME="Debian GNU/Linux"
VERSION_ID="9"
VERSION="9 (stretch)"
VERSION_CODENAME=stretch
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
```
