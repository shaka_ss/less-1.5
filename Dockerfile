FROM debian:9

RUN apt update && apt install -y apt-transport-https ca-certificates curl gnupg
RUN echo "deb [arch=amd64] https://download.docker.com/linux/debian $(cat /etc/os-release | grep VERSION_CODENAME | awk -F= '{print $2}') stable" >> /etc/apt/sources.list.d/sources.list
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
RUN apt update && apt install -y docker-ce-cli docker-ce
RUN /usr/bin/containerd 2>/dev/null & /usr/bin/dockerd --containerd=/run/containerd/containerd.sock
